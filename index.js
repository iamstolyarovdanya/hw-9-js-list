function createItems(arr, body = document.body) {
    const ul = document.createElement("ul");
    body.appendChild(ul);
    arr.forEach(elem => {
        const li = document.createElement("li");
        li.textContent = elem;
        ul.appendChild(li);
    })
}
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 12, ];
createItems(array)
